import React from "react"
// import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import FirstPage from '../views/firstPage.js'
import SecondPage from '../views/secondPage.js'
import Login from "../components/login";
// import ThirdPage from '../views/thirdPage.js'
// import FourthPage from '../views/fourthPage.js'

const login = async( values ) => {
  const response = await (await fetch(`localhost:8001/login/${values.email}/${values.password}`)).json();

  if(response.status == 'OK'){
      <Link to="../views/fourthPage"/>
  }
}

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <FirstPage />
    <SecondPage />
    <Login login={login}/>
    {/* 
    <ThirdPage />
    <FourthPage /> 
    */}

  </Layout>
)

export default IndexPage;
