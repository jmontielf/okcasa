import React from "react"
import Button from '../components/button'

import './css/firstPage.css'
import logo from '../images/underCons.png';

const firstPage = () => (
<div className="Page1">
    <div>
        <span className="dot"></span>
        <img src={logo} alt="Logo" className="Logo" />
    </div>

    <div className="InfoContainer">
        <span className="Title">Descansa y deja la inspección <br/> en nuestras manos</span>
        <div className="ButtonContainer">
            <Button>Contáctanos</Button>
            <Button fullBtn>Reserva Hoy</Button>
        </div>
    </div>
</div>
)

export default firstPage
