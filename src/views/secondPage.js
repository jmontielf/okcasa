import React from "react"
// import Button from '../components/button'
import logo1 from '../images/icons/iconos01.png'
import logo2 from '../images/icons/iconos02.png'
import logo5 from '../images/icons/iconos03.png'
import logo4 from '../images/icons/iconos04.png'
import logo3 from '../images/icons/iconos05.png'
import logo6 from '../images/icons/iconos06.png'
import './css/secondPage.css'

const secondPage = () => (
<div className="Page2">
    <h3 className="Title">Nuestros Servicios</h3>

    <div style={{ display: 'grid', gridTemplateColumns: '2fr 3fr', paddingTop: '30px' }}>
        <div style={{ justifyContent: 'center', display: 'flex', alignItems: 'center', }}>
            <div style={{ width: '40%', }}>
                <p style={{ color: '#3F3D56' }}><b>OK</b>Casa Inspecciona propiedades nuevas, con el fin de que la recibas en perfectas condiciones y hagas valer las garantías asociadas a tu compra. <br/> <br/>Resguardando la seguridad de tu familia y el valor de tu inversión.</p>
            </div>
        </div>
        <div className="servicesContainer">
            {/* Servicio 1 */}
            <div className="serviceContainer">
                <img src={logo1} alt="Logo2" className="Logo" />
                <p className="mainTitle">Dimensiones</p>
                <p className="subTitle">5% presenta diferencias</p>
            </div>
            {/* Servicio 2 */}
            <div className="serviceContainer">
                <img src={logo2} alt="Logo2" className="Logo" />
                <p className="mainTitle">Fugas de gas</p>
                <p className="subTitle">5% presenta fugas</p>
            </div>
            {/* Servicio 3 */}
            <div className="serviceContainer">
                <img src={logo3} alt="Logo3" className="Logo" />
                <p className="mainTitle">Termografía</p>
                <p className="subTitle">5% presentan puentes térmicos</p>
            </div>
            {/* Servicio 4 */}
            <div className="serviceContainer">
                <img src={logo4} alt="Logo4" className="Logo" />
                <p className="mainTitle">Instalaciones sanitarias</p>
                <p className="subTitle">80% presenta filtraciones</p>
            </div>
            {/* Servicio 5 */}
            <div className="serviceContainer">
                <img src={logo5} alt="Logo5" className="Logo" />
                <p className="mainTitle">Electricidad</p>
                <p className="subTitle">50% presenta fallas</p>
            </div>
            {/* Servicio 6 */}
            <div className="serviceContainer">
                <img src={logo6} alt="Logo6" className="Logo" />
                <p className="mainTitle">Terminaciones</p>
                <p className="subTitle">100% de las casas presenta fallas</p>
            </div>
        </div>
    </div>
</div>
)

export default secondPage