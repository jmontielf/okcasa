import React from "react"
import Button from './button.js'
import logo from '../images/OKCasaLogo.png';

import Registro from './registro.js'
import Login from './login.js'

import './css/header.css'

class Header extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      registroModal : false,
      inicioSesion  : false,
    }

  }

  registrate = () => {
    if(this.state.registroModal == false){
      this.setState({
        registroModal: true,
      })
    }
  }


  login = () => {
    if(this.state.inicioSesion == false){
      this.setState({
        inicioSesion: true,
      })
    }
  }

  render(){
    return(
      <header>
        <div className="Container">
          <img src={logo} alt="Logo" />
          <div>
            {/* <Button noBorder>Planes</Button> */}
            <Button onClick={this.login}noBorder>Inicia Sesión</Button>
            {this.state.inicioSesion && <Login />}
            <Button onClick={this.registrate}>Regístrate</Button>
            {this.state.registroModal && <Registro />}
          </div>
        </div>
      </header>
    )
  }
}

export default Header
