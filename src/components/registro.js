import React from "react"
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Button from './button.js'

import './css/modal.css'
import './css/registro.css'

const Registro = () => (
    <div className='modal'>
        <div className='marco'>
            <div className='text-right'>
                {/* <Back action={props.close} /> */}
            </div>      
            <h3 id='title'>Registro</h3>
            <p id='subtitle'>Nuevo usuario</p>

            <Formik
                initialValues={{ email: '', password: '' }}
                validate={values => {
                    let errors = {};
                    if (!values.email) {
                    errors.email = 'Debe ingresar un correo';
                    } else if (
                    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                    ) {
                    errors.email = 'Invalid email address';
                    }
                    return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);
                    }, 400);
                }}
                >
                {({ isSubmitting }) => (
                    <Form style={{ display: 'flex', flexDirection: 'column' }}>
                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Nombre:</p>
                            <Field type="name" name="name" />
                            <ErrorMessage name="name" component="div" style={{color: 'red'}}/>
                        </div>

                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Email:</p>
                            <Field type="email" name="email" />
                            <ErrorMessage name="email" component="div" style={{color: 'red'}}/>
                        </div>

                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Rut:</p>
                            <Field type="number" name="rut" />
                            <ErrorMessage name="rut" component="div" style={{color: 'red'}} />
                        </div>

                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Telefono:</p>
                            <Field type="tel" name="tel" />
                            <ErrorMessage name="telefono" component="div" style={{color: 'red'}} />
                        </div>

                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Banco:</p>
                            {/* Ingresar picker */}
                            <Field type="password" name="banco" />
                            <ErrorMessage name="banco" component="div" style={{color: 'red'}} />
                        </div>

                        &nbsp;
                        <Button type="submit" disabled={isSubmitting} id='signup'>
                            Registrarme
                        </Button>
                    </Form>
                )}
            </Formik>

        </div>
    </div>
)

export default Registro
