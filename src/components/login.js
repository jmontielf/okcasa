import React from "react"
import { Formik, Form, Field, ErrorMessage } from 'formik';
import Button from './button.js'
import { Link } from 'gatsby';

import './css/modal.css'
import './css/registro.css'

const Login = () => (
    <div className='modal'>
        <div className='marco'>
            <div className='text-right'>
                {/* <Back action={props.close} /> */}
            </div>      
            <h3 id='title'>Inicio Sesión</h3>
            <p id='subtitle'>¡Bienvenido!</p>
            <Formik
                initialValues={{ email: '', password: '' }}
                validate={values => {
                    let errors = {};
                    if (!values.email) {
                        errors.email = 'Debe ingresar un correo';
                    }

                    return errors;
                }}
                onSubmit={(values, { setSubmitting }) => {
                    setTimeout(() => {
                    this.props.login( values )
                    setSubmitting(false);
                    }, 400);
                }}
                >
                {({ isSubmitting }) => (
                    <Form style={{ display: 'flex', flexDirection: 'column' }}>
                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Rut:</p>
                            <Field type="text" name="email" />
                            <ErrorMessage name="email" component="div" style={{color: 'red'}}/>
                        </div>

                        <div style={{ padding: '6px 0px 6px 0px' }}>
                            <p>Contraseña:</p>
                            <Field type="password" name="password" />
                            <ErrorMessage name="password" component="div" style={{color: 'red'}}/>
                        </div>
                        &nbsp;
                        <Button type="submit" disabled={isSubmitting} id='signup'>
                            Iniciar Sesión
                        </Button>
                    </Form>
                )}
            </Formik>
        </div>
    </div>
)

export default Login
