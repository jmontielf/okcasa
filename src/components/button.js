import styled, { css } from 'styled-components'

const Button = styled.button `
  font-family: TRYClother-Regular;
  background: transparent;
  border-radius: 0px;
  border: 1px solid #DC7406;
  color: #DC7406;
  margin: 0 1em;
  padding: 5px;

  ${props =>
    props.fullBtn &&
    css`
      background: #DC7406;
      color: white;
    `};

  ${props =>
    props.noBorder &&
    css`
        border: none;
    `}

    
`

export default Button;